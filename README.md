# README

## Project Overview

This repository serves as a Makefile starter kit, designed to help you get started with Visual Studio Code (VSC), Makefiles, and cross-platform development. Whether you're on Windows using MSYS2 or on Linux/macOS using Bash, this guide will help you automate the build process for C++ projects. It’s ideal for developers looking to streamline C++ development and learn the basics of using Makefiles with Visual Studio Code across different operating systems.

## What is this Repository For?

- Learning the basics of Makefiles: Understand the structure, syntax, and functionality of Makefiles.
- Setting up Visual Studio Code for C++ development: Prepare your IDE to effectively build and debug C++ projects.
- Automating builds: Learn how to simplify the compilation process by using Make to automate complex workflows.

## Key Learning Goals

By the end of this tutorial, you will:

- Understand the structure and components of a Makefile.
- Be able to create and execute a simple Makefile for compiling and linking C++ programs.
- Automate routine tasks like cleaning up build artifacts and running executables.
- Get comfortable with setting up Visual Studio Code for C++ development.

## Key Concepts

Before diving into the Makefile examples, it's helpful to understand a few key concepts:

- Targets: A target is usually a file that is generated by a program or a label for a group of commands to run. Targets appear at the beginning of a Makefile entry, followed by a colon.

- Dependencies: Each target can have a set of dependencies—files or targets that must exist before the target can be processed. If a dependency has been updated more recently than the target, the Makefile will rebuild the target.

- Recipes: The recipe is the set of commands that make uses to build a target. Commands must be indented with a tab (not spaces). The recipe runs if the target is out of date or missing.

- Variables: You can define variables to avoid repeating yourself within a Makefile. Variables help make your Makefiles more maintainable.


# Quick Start Guide for MSYS2 on Windows

### Step 1: Install MSYS2

- Download and install MSYS2 from [here](https://www.msys2.org/).

### Step 2: Install Necessary Packages via MSYS Bash (UCRT64)

- Open **MSYS2 UCRT64 Bash** and install the following packages:

    ```bash
    pacman -S git
    pacman -S make
    pacman -S mingw-w64-ucrt-x86_64-gcc
    pacman -S gdb
    ```

### Step 3: Set Up Project Structure

1. Open **MSYS2 UCRT64 Bash**.
2. Navigate to the directory where you want to create your project. For example, if you want to create it in your home directory, use:

    ```bash
    cd ~
    ```

3. Create a new directory named `projects`:

    ```bash
    mkdir projects
    ```

4. Navigate into the `projects` directory:

    ```bash
    cd projects
    ```

5. Inside the `projects` directory, create a `lobby` directory. Within the 'lobby' directory create two more directories: `src` and `include`:

    ```bash
    mkdir lobby
    cd lobby
    mkdir src include
    ```

### Step 4: Create a Simple "Hello GPP I" Makefile

- Inside your `projects` directory, create a file named `Makefile` (using a text editor such as `nano`):

    ```bash
    nano Makefile
    ```

- Add the following content to the `Makefile`:

    ```makefile
    hello:
        echo "Hello Gameplay Programming I"
    ```

- Press `Ctrl+O` to save the file and `Ctrl+X` to exit the editor.

- Run `make hello` to see the output:

    ```bash
    make hello
    ```

- You should see the following output:

    ```bash
    Hello Gameplay Programming I
    ```

### Step 5: Create and Compile a `lobby.cpp` File

1. Inside the `src` directory, create a file named `lobby.cpp`:

    ```bash
    nano ./src/lobby.cpp
    ```

2. Add the following content to `lobby.cpp`:

    ```cpp
    #include <iostream>

    using namespace std;

    int main() {
        cout << "Welcome to the Gameplay Programming I lobby!" << endl;
        return 0;
    }
    ```

3. Save the file and exit the editor by pressing `Ctrl+O`, then `Ctrl+X`.

4. To compile the file, run the following command in the terminal:

    ```bash
    g++ ./src/lobby.cpp
    ```

5. After compiling, an executable file named `a.exe` (on Windows) will be generated. Run the program:

    ```bash
    ./a.exe
    ```

6. You should see the following output:

    ```bash
    Welcome to the Gameplay Programming I lobby!
    ```

### Step 6: Add a C++ Header File and Update Makefile

1. **Create a C++ Header File**  
   Inside the `include` directory, create a file named `lobby_welcome.h`:

    ```bash
    nano ./include/lobby_welcome.h
    ```

2. Add the following content to `lobby_welcome.h`:

    ```cpp
    #ifndef LOBBY_WELCOME_H
    #define LOBBY_WELCOME_H

    #include <string>

    std::string lobbyWelcomeMessage();

    #endif
    ```

3. Save and exit the editor.

4. **Create a Corresponding C++ Source File**  
   Inside the `src` directory, create a file named `lobby_welcome.cpp`:

    ```bash
    nano ./src/lobby_welcome.cpp
    ```

5. Add the following content to `lobby_welcome.cpp`:

    ```cpp
    #include "lobby_welcome.h"

    std::string lobbyWelcomeMessage() {
        return "Waiting for players to join the lobby...";
    }
    ```

6. Save and exit the editor.

7. **Update `lobby.cpp`**  
   Modify `lobby.cpp` to use the new `lobby_welcome.hpp` header file:

    ```bash
    nano ./src/lobby.cpp
    ```

8. Replace the content of `lobby.cpp` with the following:

    ```cpp
    #include <iostream>
    #include "lobby_welcome.hpp"

    using namespace std;

    int main() {
        cout << "Welcome to the Gameplay Programming I lobby!" << endl;
        cout << lobbyWelcomeMessage() << endl;
        return 0;
    }
    ```

9. Save and exit the editor.

### Step 7: Update the Makefile to Compile Multiple Files

- Modify the `Makefile` to include both `lobby.cpp` and `lobby_welcome.cpp`:

    ```bash
    nano Makefile
    ```

- Replace the content of the `Makefile` with the following:

    ```makefile
    CXX := g++
    TARGET := lobby
    SRC := ./src/lobby.cpp ./src/lobby_welcome.cpp
    INC := -I./include

    all: $(TARGET)

    $(TARGET): $(SRC)
        $(CXX) $(INC) -o $(TARGET) $(SRC)

    clean:
        rm -f $(TARGET)
    ```

- Save and exit the editor.

### Step 8: Compile and Run the Program

1. In the terminal, run `make` to compile the project:

    ```bash
    make
    ```

2. After compiling, an executable named `lobby` will be generated. Run the program:

    ```bash
    ./lobby
    ```

3. The output should be:

    ```bash
    Welcome to the Gameplay Programming I lobby!
    Waiting for players to join the lobby...
    ```

Here's a modified guide to set up a similar C++ development environment on Linux:

# Quick Start Guide for C++ Development on Linux

### Step 1: Install Necessary Packages

- Open your terminal.
- Install essential packages for C++ development. Use the package manager specific to your Linux distribution.

  For Debian-based systems (like Ubuntu):
  ```bash
  sudo apt update
  sudo apt install build-essential gdb git
  ```

  For Red Hat-based systems (like Fedora):
  ```bash
  sudo dnf groupinstall 'Development Tools'
  sudo dnf install gdb git
  ```

### Step 2: Set Up Project Structure

1. Open your terminal.
2. Navigate to the directory where you want to create your project. For example, if you want to create it in your home directory, use:

    ```bash
    cd ~
    ```

3. Create a new directory named `projects`:

    ```bash
    mkdir projects
    ```

4. Navigate into the `projects` directory:

    ```bash
    cd projects
    ```

5. Inside the `projects` directory, create a `lobby` directory. Within the 'lobby' directory, create two more directories: `src` and `include`:

    ```bash
    mkdir lobby
    cd lobby
    mkdir src include
    ```

### Step 3: Create a Simple "Hello GPP I" Makefile

- Inside your `projects` directory, create a file named `Makefile` (using a text editor such as `nano`):

    ```bash
    nano Makefile
    ```

- Add the following content to the `Makefile`:

    ```makefile
    hello:
        @echo "Hello Gameplay Programming I"
    ```

- Press `Ctrl+O` to save the file and `Ctrl+X` to exit the editor.

- Run `make hello` to see the output:

    ```bash
    make hello
    ```

- You should see the following output:

    ```bash
    Hello Gameplay Programming I
    ```

### Step 4: Create and Compile a `lobby.cpp` File

1. Inside the `src` directory, create a file named `lobby.cpp`:

    ```bash
    nano ./src/lobby.cpp
    ```

2. Add the following content to `lobby.cpp`:

    ```cpp
    #include <iostream>

    using namespace std;

    int main() {
        cout << "Welcome to the Gameplay Programming I lobby!" << endl;
        return 0;
    }
    ```

3. Save the file and exit the editor by pressing `Ctrl+O`, then `Ctrl+X`.

4. To compile the file, run the following command in the terminal:

    ```bash
    g++ ./src/lobby.cpp -o lobby
    ```

5. After compiling, an executable file named `lobby` will be generated. Run the program:

    ```bash
    ./lobby
    ```

6. You should see the following output:

    ```bash
    Welcome to the Gameplay Programming I lobby!
    ```

### Step 5: Add a C++ Header File and Update Makefile

1. **Create a C++ Header File**  
   Inside the `include` directory, create a file named `lobby_welcome.h`:

    ```bash
    nano ./include/lobby_welcome.h
    ```

2. Add the following content to `lobby_welcome.h`:

    ```cpp
    #ifndef LOBBY_WELCOME_H
    #define LOBBY_WELCOME_H

    #include <string>

    std::string lobbyWelcomeMessage();

    #endif
    ```

3. Save and exit the editor.

4. **Create a Corresponding C++ Source File**  
   Inside the `src` directory, create a file named `lobby_welcome.cpp`:

    ```bash
    nano ./src/lobby_welcome.cpp
    ```

5. Add the following content to `lobby_welcome.cpp`:

    ```cpp
    #include "lobby_welcome.h"

    std::string lobbyWelcomeMessage() {
        return "Waiting for players to join the lobby...";
    }
    ```

6. Save and exit the editor.

7. **Update `lobby.cpp`**  
   Modify `lobby.cpp` to use the new `lobby_welcome.h` header file:

    ```bash
    nano ./src/lobby.cpp
    ```

8. Replace the content of `lobby.cpp` with the following:

    ```cpp
    #include <iostream>
    #include "lobby_welcome.h"

    using namespace std;

    int main() {
        cout << "Welcome to the Gameplay Programming I lobby!" << endl;
        cout << lobbyWelcomeMessage() << endl;
        return 0;
    }
    ```

9. Save and exit the editor.

### Step 6: Update the Makefile to Compile Multiple Files

- Modify the `Makefile` to include both `lobby.cpp` and `lobby_welcome.cpp`:

    ```bash
    nano Makefile
    ```

- Replace the content of the `Makefile` with the following:

    ```makefile
    CXX := g++
    TARGET := lobby
    SRC := ./src/lobby.cpp ./src/lobby_welcome.cpp
    INC := -I./include

    all: $(TARGET)

    $(TARGET): $(SRC)
        $(CXX) $(INC) -o $(TARGET) $(SRC)

    clean:
        rm -f $(TARGET)
    ```

- Save and exit the editor.

### Step 7: Compile and Run the Program

1. In the terminal, run `make` to compile the project:

    ```bash
    make
    ```

2. After compiling, an executable named `lobby` will be generated. Run the program:

    ```bash
    ./lobby
    ```

3. The output should be:

    ```bash
    Welcome to the Gameplay Programming I lobby!
    Waiting for players to join the lobby...
    ```

>The above guides provide a step-by-step process for setting up a C++ development environment using MSYS2 on Windows and C++ Development on Linux, organising your project, and compiling code using Makefiles to simulate a simple game lobby experience.

---

## Visual Studio Code Setup

1. **Download Visual Studio Code**  
   Visit the [Visual Studio Code website](https://code.visualstudio.com/) and download the appropriate version for your operating system.

2. **Install the C/C++ Extension for Visual Studio Code**  
   Open Visual Studio Code, press `Ctrl+Shift+X`, and search for `@ext:ms-vscode.cpptools`. Install the ["C/C++ IntelliSense, debugging, and code browsing" extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools).

3. **Install Build Tools**  
   - **Linux**: Open a terminal and run:  
     ```bash
     sudo apt install build-essential gdb
     ```  
     This command installs the essential build tools and the GDB debugger.
   - **Windows**: Refer to the section on [Using GCC with MinGW](https://code.visualstudio.com/docs/cpp/config-mingw).

4. **Create Makefiles and Source Files**  
   Refer to Parts 1 to 11 below for an introduction to creating Makefiles.

5. **Experiment with Makefiles**  
   Try out different Makefiles and syntax variations to understand how they work.

## Visual Studio Code Configuration Guidelines

- *Ensure Correct Visual Studio Code Settings*: Make sure that tabs are not replaced by spaces in your Makefile. If you encounter an error like `Makefile:2: *** missing separator. Stop.`, check your tab settings in VSC. More information can be found on [Stackoverflow](https://stackoverflow.com/questions/23927212/makefile2-missing-separator-stop)
- Ensure Visual Studio Code is setup correcly. A typical error is that 'Tab's is set to spaces instead of being stored as tabs.

<img src="./images/MakeFileSuccess.png" alt="Makefile execution success" width="250px">

---

## Parts 1 to 11: Introduction to Makefiles

### Part 1: Verifying Setup

**Objective:** Ensure your environment is correctly set up to handle Makefile syntax with proper use of tabs.

Use the sample Makefile below to check if your environment is correctly set up. Save this file as `Makefile` or `makefile`:

**Sample Makefile:**

```makefile
hello:
    echo "Checking that tabs are set up correctly"
```

**Steps:**

1. Save the file as `Makefile` or `makefile`.
2. Run `make` in the terminal to invoke this Makefile
   ```bash
   make
   ```

**Explanation:**

   Makefiles require tabs for indentation rather than spaces. This is essential since using spaces will cause the error missing separator. Stop.

**Tips for Debugging:**

   Ensure your editor (like Visual Studio Code) is using tabs instead of spaces. You can enable whitespace rendering in VS Code by searching for Editor: Render Whitespace in the settings to visually check if tabs are being used.

**Common Error:**

   If make fails, you likely have spaces instead of tabs. Ensure your tab settings are correct and try again.


### Part 2: Compile Using g++

**Objective:** Learn how to compile a simple C++ program using the g++ compiler.

**Sample Command:**

To compile a C++ program, use:

```bash
g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
```

**Explanation of Options:**

- **`g++`**: Invokes the C++ compiler from the GNU Compiler Collection (GCC).  
- **`-o`**: Specifies the output file name for the compiled executable.  
- **`sampleapp`**: The output executable name.  
- **`./src/main.cpp`**: The main source file.  
- **`./src/sample_function.cpp`**: Additional dependency.  
- **`-I.`**: Includes the current directory for header files.  

**Suggested File Structure:**

```plaintext
project_root/
├── include/
│   └── sample_include.h
├── src/
│   ├── main.cpp
│   └── sample_function.cpp
└── Makefile
```

**Example C++ Code:**

main.cpp:

```cpp
#include <iostream>
#include "sample_function.h"

int main() {
    sample_function();
    return 0;
}
```

sample_function.cpp:

```cpp
#include <iostream>

void sample_function() {
    std::cout << "Hello from sample function!" << std::endl;
}
```

**Important:**

Make sure you include all source files in the compilation command. Also, try running with different options like '-g' for debugging symbols.

### Part 3: Generate Object Files

**Objective:** Understand the process of creating object files (.o) as an intermediate compilation step.

**Sample Command:**

```bash
g++ -c ./src/main.cpp ./src/sample_function.cpp -I.
```

**Explanation:**

   `-c`: Compiles the source files into object files but does not link them into an executable. The resulting files, main.o and sample_function.o, are intermediate files containing machine code but not yet linked.

**Suggestion:**

Open the `.o` files in a hex editor or inspect them using the objdump command to understand the content of object files.

### Part 4: Produce Assembly Files

**Objective:** Generate assembly language files from your C++ code.

**Sample Command:**

```bash
g++ -S ./src/main.cpp ./src/sample_function.cpp -I.
```

**Explanation:**
   `-S`: Generates assembly code from the source files and produces `.s` files.
    You can open these `.s` files to view the assembly instructions generated by the compiler.

**Improvement:**

Experiment with different optimisation levels by adding flags like `-O2` or `-O3` to see how the generated assembly code changes:

### Part 5: Enabling All Warnings

**Objective:** Learn to compile with all warnings enabled for better code quality.

**Sample Command:**

```bash
g++ -Wall -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
```

**Explanation:**

   `-Wall`: Enables all compiler warnings, helping to catch potential issues early.

**Improvement:**

   Add `-Wextra` and `-Werror` flags to catch more warnings and treat them as errors:

**Sample Command:**

```makefile
g++ -Wall -Wextra -Werror -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
```

**Tip:**

Regularly compile with warnings enabled to maintain a high level of code quality and avoid bugs.

### Part 6: Basic Makefile Structure

**Objective:** Create a simple Makefile that compiles your program.

**Sample Makefile:**

```makefile
build:
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
```

**Explanation:**

   This basic Makefile defines a build target that compiles the program.

### Part 7: Adding Progress Output

**Objective:** Add user feedback during the build process.

```makefile
build:
    @echo "Build Started"
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
    @echo "Build Complete"
```

**Explanation:**

   `@echo`: Outputs progress messages to the terminal without showing the commands being executed.

**Tip:**

Add colored output for better visibility

```makefile
build:
    @echo "Build Started"
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
    echo "\033[0;32mBuild Complete\033[0m"
```


### Part 8: Setting Default Goal

**Objective:** Set a default target that runs when no target is specified.

**Sample Makefile:**

```makefile
.DEFAULT_GOAL := build

build:
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
```

[![](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVEQ7XG4gICAgQVthbGw6XSAtLSBDYWxscyBUYXJnZXQgLS0-IEJbYnVpbGQ6XVxuICAiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlLCJhdXRvU3luYyI6dHJ1ZSwidXBkYXRlRGlhZ3JhbSI6ZmFsc2V9)](https://mermaid-js.github.io/mermaid-live-editor/edit#eyJjb2RlIjoiZ3JhcGggVEQ7XG4gICAgQVthbGw6XSAtLSBDYWxscyBUYXJnZXQgLS0-IEJbYnVpbGQ6XVxuICAiLCJtZXJtYWlkIjoie1xuICBcInRoZW1lXCI6IFwiZGVmYXVsdFwiXG59IiwidXBkYXRlRWRpdG9yIjpmYWxzZSwiYXV0b1N5bmMiOnRydWUsInVwZGF0ZURpYWdyYW0iOmZhbHNlfQ)


**Explanation:**

   `.DEFAULT_GOAL`: This sets the build target as the default, so running make without any arguments will invoke the build target.

**Improvement:**

   You can experiment with changing the default goal based on your project requirements. For example, you can set all or clean as default based on your workflow.

### Part 9: Cleaning Up Files

**Objective:** Add a target to clean up intermediate files.

```makefile
clean:
    @echo "Cleaning up"
    rm *.o
```

Run `make clean` to execute this target.

```bash
make clean
```

**Explanation:**

- `rm *.o`: Deletes all object files.
- `rm sampleapp`: Deletes the executable as well.

**Improvement:**

   Use `rm -f` to avoid confirmation prompts and ignore errors if files don't exist

```makefile
clean:
    @echo "Cleaning up"
    rm -f *.o sampleapp
```

### Part 10: Complete Simple Makefile

**Objective:** Create a complete Makefile that builds, runs, and cleans up your project.

**Sample Makefile:**

```makefile
all: build

build:
    @echo "Build Started"
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
    @echo "Build Complete"
    ./sampleapp

clean:
    @echo "Cleaning up"
    rm *.o
```

**Improvement:**

   Automatically run the executable after building:

```makefile
build:
    @echo "Build Started"
    g++ -o sampleapp ./src/main.cpp ./src/sample_function.cpp -I.
    @echo "Build Complete"
    ./sampleapp  # Automatically run the program
``` 

### Part 11: Using Variables and Options

**Objective:** Make your Makefile more flexible using variables.

**Sample Makefile:**

```makefile
CXX         := g++
MSG_START   := "Build Started"
MSG_END     := "Build Complete"
MSG_CLEAN   := "Cleaning up"
BUILD_DIR   := ./bin
SRC_DIR     := ./src
TARGET      := ${BUILD_DIR}/sampleapp.bin
SRC         := ${SRC_DIR}/main.cpp ${SRC_DIR}/sample_function.cpp

all: build

build:
    @echo ${MSG_START}
    ${CXX} -o ${TARGET} ${SRC} -I.
    @echo ${MSG_END}
    ./${TARGET}

clean:
    @echo ${MSG_CLEAN}
    rm ${TARGET}
```

**Improvement:**

Modularise your source and include directories for larger projects:

```makefile
SRC_DIR := ./src
INC_DIR := ./include
SRC := $(SRC_DIR)/main.cpp $(SRC_DIR)/sample_function.cpp
```

Add a debug mode option to toggle between debug and release builds:

```makefile
CXXFLAGS := -Wall
DEBUG := 1

ifeq ($(DEBUG), 1)
    CXXFLAGS += -g
else
    CXXFLAGS += -O2
endif

build:
    $(CXX) $(CXXFLAGS) -o $(TARGET) $(SRC)
```

---

## Setting Up Build Tools with MSYS2 on Windows

1. **Download MSYS2**  
   Visit [MSYS2 website](https://www.msys2.org/) and download the installer.

2. **Install and Update MSYS2**  
   - Open MSYS2 terminal and run:  
     ```bash
     pacman -Syu
     ``` 
     Follow the prompts to update the package database and base packages.

3. **Install Essential Tools**  
   Open MSYS2 terminal and run:
   - Install Git:  
     ```bash
     pacman -S git
     ```
   - Install `make`:  
     ```bash
     pacman -S make
     ```
   - Install GCC and G++:  
     ```bash
     pacman -S mingw-w64-ucrt-x86_64-gcc
     ```
   - Install GDB:  
     ```bash
     pacman -S gdb
     ```

4. **Check Installations**  
   - Open MSYS2 terminal and type `g++ --version` and `gdb --version` to verify installations.

5. **Modify System Path**  
   Add `C:\\msys64\\mingw64\\bin` to your system PATH environment variable.

6. **Configure VSC Terminal Settings**  
   Modify Visual Studio Code's terminal settings to use MSYS Bash:
   ```json
   {
        "terminal.integrated.shell.windows": "C:\\msys64\\usr\\bin\\bash.exe", 
        "terminal.integrated.shellArgs.windows": ["--login", "-i"],
        "terminal.integrated.env.windows": { 
             "MSYSTEM": "MINGW64", 
             "CHERE_INVOKING": "1"
        }
   }
   ```

---

## Debugging and Common Issues

### Common Problems with Makefiles

When working with Makefiles, you may encounter some common errors:

- Missing Separator Error: This occurs when tabs are replaced with spaces. Ensure that each command in the recipe starts with a tab.
- Undefined Target: This happens when the target name doesn't match any file or label in the Makefile. Double-check your target and dependency names.
- Outdated Files: Sometimes a target won't rebuild even when the files have changed. Make sure that the dependencies are listed correctly and have newer timestamps than the target.
- Path Issues on Windows: If you’re using MSYS2 on Windows, make sure your paths are correctly configured. Inconsistent paths can lead to issues when invoking compilers or other tools.

### Debugging Tips

- Verbose Mode: Use the make -n command to print what make would do without actually executing the commands. This can help trace issues with your targets and dependencies.
- Checking Variables: Use make -p to print all variables and rules, which can help identify unexpected values or rules being applied.

---

## Essential Bash (Bourne Again SHell) Commands Guide

Bash (Bourne Again SHell) is a powerful command-line interpreter or "shell" commonly used in Unix-like operating systems such as [Linux](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview) and [macOS](https://support.apple.com/en-gb/guide/terminal/welcome/mac). It equips users with the ability to interact with the operating system by typing commands, running programs, manipulating files, and automating tasks through scripting.

Bash is not limited to Unix-like operating systems. It's also available on Windows through environments like [MSYS2](https://www.msys2.org/) and [Git Bash](https://git-scm.com/downloads), and [WSL (Windows Subsystem for Linux)](https://learn.microsoft.com/en-us/windows/wsl/install). This accessibility makes it a great choice for Windows users who want to work with Unix-style commands and tools, providing a Unix-like interface within Windows and allowing users to run Bash commands and scripts.

<img src="./images/BashCommands.png" alt="Bash Command" width="250px">

1. **`pwd`** - Print Working Directory

      **Description**: Displays the path of the current working directory.

      **Usage**:
      ```bash
      pwd
      ```

      **Example**:
      ```bash
      $ pwd
      /c/Users/Username/projects
      ```


2. **`ls`** - List Directory Contents

      **Description**: Lists files and directories in the current directory.

      **Usage**:
      ```bash
      ls
      ```
      **Options**:
      - `-l`: Long format, displays detailed information.
      - `-a`: Shows all files, including hidden ones.
      - `-h`: Human-readable format (e.g., 1K, 234M).

      **Examples**:
      ```bash
      $ ls
      main.cpp sample_function.cpp
      
      $ ls -l
      total 8
      -rw-r--r-- 1 username username 1234 Sep 16 10:00 main.cpp
      -rw-r--r-- 1 username username 1234 Sep 16 10:00 sample_function.cpp
      
      $ ls -a
      .  ..  .gitignore  main.cpp sample_function.cpp
      ```


3. **`cd`** - Change Directory

      **Description**: Changes the current working directory.

      **Usage**:
      ```bash
      cd [directory]
      ```
      **Options**:
      - `..`: Move up one directory level.
      - `~`: Move to the home directory.

      **Examples**:
      ```bash
      $ cd /c/Users/Username/projects
      $ cd ..
      $ cd ~
      ```


4. **`mkdir`** - Make Directory

      **Description**: Creates a new directory.

      **Usage**:
      ```bash
      mkdir [directory_name]
      ```
      **Example**:
      ```bash
      $ mkdir src bin
      ```


5. **`rmdir`** - Remove Directory

      **Description**: Deletes an empty directory.

      **Usage**:
      ```bash
      rmdir [directory_name]
      ```

      **Example**:
      ```bash
      $ rmdir bin
      ```


6. **`rm`** - Remove Files or Directories

      **Description**: Deletes files or directories.

      **Usage**:
      ```bash
      rm [file_name]
      ```
      **Options**:
      - `-r`: Recursive, for directories.
      - `-f`: Force, ignores non-existent files and prompts.

      **Examples**:
      ```bash
      $ rm lobby.exe
      $ rm -r bin
      ```


7. **`cp`** - Copy Files or Directories

      **Description**: Copies files or directories.

      **Usage**:
      ```bash
      cp [source] [destination]
      ```
      **Options**:
      - `-r`: Recursive, for directories.
      - `-i`: Interactive, prompts before overwrite.

      **Examples**:
      ```bash
      $ cp lobby.cpp backup_lobby.cpp
      $ cp -r bin backup_bin
      ```


8. **`mv`** - Move or Rename Files or Directories

      **Description**: Moves or renames files or directories.

      **Usage**:
      ```bash
      mv [source] [destination]
      ```

      **Examples**:
      ```bash
      $ mv lobby.cpp /c/Users/Username/other_projects_directory/
      $ mv lobby.cpp new_lobby.cpp
      ```


9. **`touch`** - Create Empty File or Update Timestamp

      **Description**: Creates an empty file or updates the timestamp of an existing file.

      **Usage**:
      ```bash
      touch [file_name]
      ```
      **Example**:
      ```bash
      $ touch lobby.cpp
      ```


10. **`cat`** - Concatenate and Display Files

      **Description**: Displays the contents of a file.

      **Usage**:
      ```bash
      cat [file_name]
      ```

      **Example**:
      ```bash
      $ cat lobby.cpp
      ```


11. **`grep`** - Search for Patterns in Files

      **Description**: Searches for specific patterns within files.

      **Usage**:
      ```bash
      grep [pattern] [file_name]
      ```

      **Example**:
      ```bash
      $ grep "main" lobby.cpp
      ```


12. **`echo`** - Display a Line of Text

      **Description**: Displays a line of text or a variable value.

      **Usage**:
      ```bash
      echo [text]
      ```

      **Example**:
      ```bash
      $ echo "Hello Gameplay Programming I!"
      ```


13. **`chmod`** - Change File Permissions

      **Description**: Changes the permissions of a file or directory.

      **Usage**:
      ```bash
      chmod [permissions] [file_name]
      ```

      **Examples**:
      ```bash
      $ chmod +x lobby.bin
      $ chmod 644 lobby.bin
      ```


14. **`chown`** - Change File Owner and Group

      **Description**: Changes the owner and/or group of a file or directory.

      **Usage**:
      ```bash
      chown [owner][:group] [file_name]
      ```

      **Example**:
      ```bash
      $ chown username:groupname lobby.bin
      ```


15. **`find`** - Search for Files in a Directory Hierarchy

      **Description**: Searches for files and directories within a directory hierarchy.

      **Usage**:
      ```bash
      find [path] [options] [expression]
      ```

      **Example**:
      ```bash
      $ find /c/Users/Username/Projects/ -name "*.cpp"
      ```


16. **`man`** - Display the Manual for a Command

      **Description**: Displays the manual page for a command.

      **Usage**:
      ```bash
      man [command]
      ```

      **Example**:
      ```bash
      $ man ls
      ```

   Most of these commands are compatible with MSYS2 running on Windows OS and should help you manage files and directories effectively. For additional details, refer to the `man` pages or use the `--help` option with each command.

---

## Useful Resources

- [GNU Make Manual](https://www.gnu.org/software/make/manual/make.html)
- [Makefile Tutorial](https://makefiletutorial.com/)
- [GDB GNU Debugger Project](https://www.gnu.org/software/gdb/)
- [Enabling Build and Debugging in Visual Studio Code](https://code.visualstudio.com/docs)
- Debugging with VSC beyond return point may result in the following error message being displayed [pop up](https://github.com/Microsoft/vscode-cpptools/issues/1123)

## Contact

For further assistance, contact:  
**Philip Bourke** - `philip [dot] bourke [at] setu [dot] ie`

